package com.retroapplication2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by syed on 28/7/17.
 */

public class ItemDatabase extends SQLiteOpenHelper {

    private static final String TAG = ItemDatabase.class.getSimpleName();
    public ItemDatabase(Context context) {
        super(context, Constant.DATABASE.DB_NAME, null, Constant.DATABASE.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {

            db.execSQL(Constant.DATABASE.CREATE_TABLE_QUERY);
        }catch (SecurityException ex){

            Log.d(TAG, ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Constant.DATABASE.DROP_QUERY);
        this.onCreate(db);
    }

    //CRUD (create read update delete)

    public void addItem(ItemObject itemObject){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constant.DATABASE.TITLE,itemObject.getSongTitle());
        contentValues.put(Constant.DATABASE.YEAR,itemObject.getSongYear());
        contentValues.put(Constant.DATABASE.AUTHOR,itemObject.getSongAuthor());

        db.insert(Constant.DATABASE.TABLE_NAME,null,contentValues);
        db.close();
    }

    public ArrayList<ItemObject> getItemList(){

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(Constant.DATABASE.GET_ITEM_QUERY,null);

        ArrayList<ItemObject> itemObjectArrayList = new ArrayList<>();
        if (cursor.getCount()>0){

            if (cursor.moveToFirst()){
                do{

                    ItemObject itemObject = new ItemObject();
                    itemObject.setSongTitle(cursor.getString(cursor.getColumnIndex(Constant.DATABASE.TITLE)));
                    itemObject.setSongYear(cursor.getString(cursor.getColumnIndex(Constant.DATABASE.YEAR)));
                    itemObject.setSongAuthor(cursor.getString(cursor.getColumnIndex(Constant.DATABASE.AUTHOR)));

                    itemObjectArrayList.add(itemObject);
                }while (cursor.moveToNext());
            }
        }

        return itemObjectArrayList;
    }
}
