package com.retroapplication2;

import android.graphics.Movie;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by syed on 28/7/17.
 */

public interface ApiInterface {

    @GET("blog/mp.php")
    Call<ArrayList<ItemObject>> getSongList();
}
