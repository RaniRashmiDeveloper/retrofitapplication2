package com.retroapplication2;

/**
 * Created by syed on 28/7/17.
 */

public class Constant {

    public static final class DATABASE {

        public static final String DB_NAME = "Item";
        public static final int DB_VERSION = 1;
        public static final String TABLE_NAME = "Item";

        public static final String DROP_QUERY = "DROP TABLE IF EXIST " + TABLE_NAME;

       public static final String GET_ITEM_QUERY = "SELECT * FROM " + TABLE_NAME;

        public static final String ITEM_ID = "itemId";
        public static final String TITLE = "title";
        public static final String YEAR = "year";
        public static final String AUTHOR = "author";


//        public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + "" +
//                "(" + ITEM_ID + " INTEGER PRIMARY KEY not null," +
//                TITLE + " TEXT not null," +
//                YEAR + " TEXT not null," +
//                AUTHOR+ " TEXT not null,";

        public static final String CREATE_TABLE_QUERY = "CREATE TABLE " + TABLE_NAME + "" +
                "(" + YEAR+ " INTEGER PRIMARY KEY not null," +
                TITLE+ " TEXT not null," +
                AUTHOR+ " TEXT not null)";
    }
}
