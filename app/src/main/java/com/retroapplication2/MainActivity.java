package com.retroapplication2;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;

import java.io.InputStream;
import java.util.ArrayList;

import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private RecyclerViewAdapter adapter;

    private ArrayList<ItemObject> songList;

    private ItemDatabase itemDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(MainActivity.this);

        if (songList == null)
        songList = new ArrayList<>();
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter(MainActivity.this,songList);
        recyclerView.setAdapter(adapter);


        if(Utils.isNetworkAvailable(getApplicationContext())){

            // if network is avilable the do network call
            requestJsonObject();

        }else {
            requestJsonObjectFromDatabase();
        }




    }

    private void requestJsonObjectFromDatabase() {
        itemDatabase = new ItemDatabase(this);
        songList = itemDatabase.getItemList();
        adapter.setData(songList);
        adapter.notifyDataSetChanged();

//        recyclerView.setAdapter(new RecyclerViewAdapter(MainActivity.this,songList));
    }

    private void requestJsonObject() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        retrofit2.Call<ArrayList<ItemObject>> call= apiService.getSongList();

        call.enqueue(new Callback<ArrayList<ItemObject>>() {
            @Override
            public void onResponse(retrofit2.Call<ArrayList<ItemObject>> call, Response<ArrayList<ItemObject>> response) {
                 songList = response.body();
                // recyclerView.setAdapter(new RecyclerViewAdapter(MainActivity.this,songList));
                if (songList!= null)
                adapter.setData(songList);
                adapter.notifyDataSetChanged();
                 itemDatabase = new ItemDatabase(MainActivity.this);
                 for (ItemObject itemObject:songList){
                     itemDatabase.addItem(itemObject);
                 }

            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<ItemObject>> call, Throwable t) {

            }
        });

    }

//    private void getRetrofitData() {
//
//        ApiInterface apiService =
//                ApiClient.getClient().create(ApiInterface.class);
//
//        retrofit2.Call<ArrayList<Movie>> call =apiService.getMovieList();
//
//        call.enqueue(new Callback<ArrayList<Movie>>() {
//            @Override
//            public void onResponse(retrofit2.Call<ArrayList<Movie>> call, Response<ArrayList<Movie>> response) {
//                ArrayList<Movie> movies = response.body();
//                Log.d(TAG, "Number of movies received: " + movies.size());
//                recyclerView.setAdapter(new CustomRvAdapter(MainActivity.this,movies));
//
//            }
//
//            @Override
//            public void onFailure(retrofit2.Call<ArrayList<Movie>> call, Throwable t) {
//                // Log error here since request failed
//                Log.e(TAG, t.toString());
//            }
//        });
//
//
//
//
//
//    }


}
